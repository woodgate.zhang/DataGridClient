# w3ajs-client

w3ajs-client is client api set used to access the services provided by DataGrid.

  - Asset Search
  - Aseset Add

## Two Classes
### DataGridReaderClient - Client for asset searching
  - **findAssets**(*queryBody*, *page*, *pageSize*, *sortField*, *order*)<br>
    *core function for asset searching, the below functions will eventally call this function*
    > **queryBody**: *Object 复合查询条件集合* <br>
    > **page**: *Integer 当前页码 整形* <br>
    > **pageSize**: *Integer 每页记录数 整形* <br>
    > **sortField**: *String 排序字段名 字符串* <br>
    > **order**: *Integer [-1,1] 升序降序*
  - **findAssetsBySize**(*fromSize*, *toSize*, *page*, *pageSize*, *sortField*, *order*)<br>
    *Search assets by asset size* <br>
    > **fromSize**: *Integer 大小下限, 整形, 单位bytes* <br>
    > **toSize**: *Integer 大小上限，整形, 单位bytes* <br>
    > **page**: *Integer 当前页码 整形* <br>
    > **pageSize**: *Integer 每页记录数 整形* <br>
    > **sortField**: *String 排序字段名 字符串* <br>
    > **order**: *Integer [-1,1] 升序降序*
  - **findAssetsByTimestamp**(*fromTime*, *toTime*, *page*, *pageSize*, *sortField*, *order*)<br>
    *Search asset by timestamp* <br>
    > **fromTime**: *Date 开始时间 日期类型* <br>
    > **toTime**: *Date 结束时间 日期类型* <br>
    > **page**: *Integer 当前页码 整形* <br>
    > **pageSize**: *Integer 每页记录数 整形* <br>
    > **sortField**: *String 排序字段名 字符串* <br>
    > **order**: *Integer [-1,1] 升序降序*
  - **findAssetsByAccount**(*accountName*, *page*, *pageSize*, *sortField*, *order*)<br>
    *Search assets by account name* <br>
    > **accountName**: *String 账户名称 字符串* <br>
    > **page**: *Integer 当前页码 整形* <br>
    > **pageSize**: *Integer 每页记录数 整形* <br>
    > **sortField**: *String 排序字段名 字符串* <br>
    > **order**: *Integer [-1,1] 升序降序*
  - **findAssetsByBlock**(*block*, *page*, *pageSize*, *sortField*, *order*)<br>
    *Search assets by block number* <br>
    > **block**: *Integer 区块号 整形* <br>
    > **page**: *Integer 当前页码 整形* <br>
    > **pageSize**: *Integer 每页记录数 整形* <br>
    > **sortField**: *String 排序字段名 字符串* <br>
    > **order**: *Integer [-1,1] 升序降序*


### DataGridWriterClient - Client for asset adding
  - **addFileAsset**(*filePath*, *account*, *metadata*, *privateKey*, *ipfsOptions*)<br>
    *add file asset into grid* <br>
    > **filePath**: *String 文件路径 字符串* <br>
    > **account**:  *String 账户名称 字符串* <br>
    > **metadata**: *String asset描述数据 字符串* <br>
    > **privateKey**:   *String 账户私钥 字符串* <br>
    > **ipfsOptions**:  *String ipfs选项 字符串* 
  - **addDataAsset**(*data*, *account*, *metadata*, *privateKey*, *ipfsOptions*) <br>
    *add data asset into grid* <br>
    > **data**: *JSON  asset数据内容 JSON对象* <br>
    > **account**:  *String 账户名称 字符串* <br>
    > **metadata**: *JSON asset描述数据 JSON对象* <br>
    > **privateKey**:   *String 账户私钥 字符串* <br>
    > **ipfsOptions**:  *JSON ipfs选项 JSON对象* 

### Installation

w3ajs-client requires [Node.js](https://nodejs.org/) v8+ to run.
Assume project root directory is "demo".
```sh
$ cd demo
$ npm i w3ajs-client --save
```
### Code Sample - *Asset searching*

```js
// import w3ajs-client module
import { DataGridReaderClient, DataGridWriterClient } from 'w3ajs-client';
// initial a new reader client instance
const client = new DataGridReaderClient('127.0.0.1', 3003);
// method - findAssetsBySize 按照资产大小查找
// assets with size not less than 70
// 大小不小于70 Bytes的资产
client.findAssetsBySize(70).then(ret => {
    console.log("server response: ", JSON.stringify(ret));
}).catch(error => {
    console.log(error);
});
// method - findAssetsBySize 按照资产大小查找
// assets with size between 10 and 100 sort by field 'size' with order 'desc'
// 大小介于10到100之间的资产，页数第一页，每页15条记录，按照size字段归类，降序
client.findAssetsBySize(10, 100, 1, 15, 'size', -1).then(ret => {
    console.log("server response: ", JSON.stringify(ret));
}).catch(error => {
    console.log(error);
});
// method - findAssetsByTimestamp 按照时间戳查找
// assets with timestamp after 2018-09-11  
// other parameters with default value: page 1, pageSize 10, sortField 'timestamp' order desc
// 2018年9月11号之后的资产，页数第1页，每页10条记录，按照timestamp字段归类，降序
client.findAssetsByTimestamp(new Date('2018-09-11')).then(ret => {
    console.log("server response: ", JSON.stringify(ret));
}).catch(error => {
    console.log(error);
});
// method - findAssetsByTimestamp 按照时间戳查找
// assets with timestamp between 2018-09-11 and 2018-09-31
// other parameters: page 2, pageSize 10, sortField 'size' order desc
// 2018年9月11到2018-09-31之间的资产，页数第2页，每页10条记录，按照size字段归类，降序
client.findAssetsByTimestamp(new Date('2018-09-11'), new Date('2018-09-31'), 2, 10, 'size', -1).then(ret => {
    console.log("server response: ", JSON.stringify(ret));
}).catch(error => {
    console.log(error);
});
// method - findAssetsByAccount 按照账户名称查找资产
// assets whose owner name contains 'ming'
// other parameters with default value: page 1, pageSize 10, sortField 'timestamp' order desc
client.findAssetsByAccount('ming').then(ret => {
    console.log("server response: ", JSON.stringify(ret));
}).catch(error => {
    console.log(error);
});
```

### Code Sample - *File Asset adding*

```js
// import w3ajs-client module
import { DataGridReaderClient, DataGridWriterClient } from 'w3ajs-client';
const privateKey = '5K1ZE5seSS3PdxWHjXsRreqtTBEjMBPrL4WTke6Fe7jwf17rHII';
// initial a new writer client instance
const client = new DataGridWriterClient('127.0.0.1', 3002);
// call the method - addFileAsset
const filePath = path.resolve(__dirname, '../../resource/test-file.txt');
const metadata = {"name": "test-file", "desc":"spaceX third launch", "location": "Beijing"};
const ipfsOptions = {"onlyHash": false};
client.addFileAsset(
    filePath, 
    'mingqi',
    JSON.stringify(metadata),
    privateKey,
    JSON.stringify(ipfsOptions)
).then(ret => {
    console.log("server response: ", JSON.stringify(ret));
}).catch(error => {
    console.log(error);
});
```

### Code Sample - *Data Asset adding*

```js
// import w3ajs-client module
import { DataGridReaderClient, DataGridWriterClient } from 'w3ajs-client';
const privateKey = '5K1ZE5seSS3PdxWHjXsRreqtTBEjMBPrL4WTke6Fe7jwf17rHII';
// initial a new writer client instance
const client = new DataGridWriterClient('127.0.0.1', 3002);
// call the method - addDataAsset
const data = {"name":"wangpengpeng","gender":"male","age":28,'random':Date.now() + Math.random().toString(36).substring(7)};
const metadata = {"name": "wangpengpeng", "desc":"spaceX test", "location": "Beijing"};
const ipfsOptions = {"onlyHash": false};
client.addDataAsset(
    JSON.stringify(data),
    'mingqi',
    metadata,
    privateKey,
    ipfsOptions
).then(ret => {
    console.log("server response: ", JSON.stringify(ret));
}).catch(error => {
    console.log(error);
});
```
### Search Result Sample

```json
{
    "docs": [
      {
        "_id": "5b98e3af69ee5919e6715e38",
        "block": 13984385,
        "thxId": "e25a56c5d45a6f7eb081299c7f628722696846fab7e23e6ba09f6e0f69a4877e",
        "account": "mingqi",
        "hashId": "QmUHhQrabcfeJqmtgn8DBWyR5qBHtmgi3a2DTfjctVDtiZ",
        "extra": {
            "name": "test.txt",
            "desc": "spaceX third launch",
            "location": "Kennedey"
        },
        "timestamp": "2018-09-11T23:37:19.500Z",
        "size": 70,
        "__v": 0
      },
      {
        "_id": "5b9b70d9974c95462414d1ba",
        "block": 14172244,
        "thxId": "802db0f8323337f34996989269b23284babab227574a10e80b3006975e8d582c",
        "account": "mingqi",
        "hashId": "QmVDF3sRADavmo2mHgG6wDn2r4j39oWp2UxEqX8kJUgLfx",
        "extra": {
            "name": "test.txt",
            "desc": "spaceX third launch",
            "location": "Kennedey"
        },
        "timestamp": "2018-09-13T02:08:05.500Z",
        "size": 70,
        "__v": 0
      }
    ],
    "total": 2,
    "limit": 10,
    "page": 1,
    "pages": 1
}
```

### Todos

 - Write MORE Tests
 - Add Asset Metadata Update

License
----

MIT