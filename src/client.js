'use strict';

import fs from 'fs';
import request from 'request';
import empty from "is-empty";
import ecc from 'eosjs-ecc';
import jsonSize from 'json-size';
import Config from './config';

const maxSizeOfMetadataByBytes = Config.asset.metadata.sizeLimit.max * 1024;

/**
 * DataGridReaderClient provides different interfaces
 * to search assets database.
 */
class DataGridReaderClient {
    /**
     * creates a instance of DataGridReaderClient.
     * @param {string} host - server ip address.
     * @param {number} port - server port number.
     */
    constructor(host, port) {
        console.log("DataGridReaderClient > constructor");
        /**
         * server ip address.
         * @type {string} host.
         * @public
         */
        this.host = host;
        /**
         * server port number.
         * @type {number} port.
         * @public
         */
        this.port = port;
    }
    /**
     * search assets by combined query conditions.
     * @param {Object} queryBody - query conditions.
     * @param {number} page=1 - page number.
     * @param {number} pageSize=10 - the number of documents in each page.
     * @param {string} sortField='timestamp' - field used to sort documents.
     * @param {number} order=[-1,1] - sort order.
     * @returns {Object} documents with pagination
     */
    findAssets(queryBody, page=1, pageSize=10, sortField='timestamp', order=1) {
        console.log("findAssets>");
        console.log("queryBody: ", JSON.stringify(queryBody));
        const queryUrl = 'http://' + this.host +':'  + this.port + '/find?page=' + page + '&pageSize=' + pageSize + '&sortBy=' + sortField + '&order=' + order;
        return new Promise((resolve, reject) => {
            request.post({
                url: queryUrl,
                json: true, // tell http return json instead of string
                body: queryBody
            }, function optionalCallback(err, httpResponse, body) {
                if (err) {
                    reject(err);
                }
                resolve(body);
            });
        });
    };
    /**
     * search assets within size span.
     * @param {number} fromSize=0 - min size.
     * @param {number} toSize - max size.
     * @param {number} page=1 - page number.
     * @param {number} pageSize=10 - the number of documents in each page.
     * @param {string} sortField='timestamp' - field used to sort documents.
     * @param {number} order=[-1,1] - sort order.
     * @returns {Object} documents with pagination
     */
    findAssetsBySize(fromSize=0, toSize=Number.MAX_SAFE_INTEGER, page=1, pageSize=10, sortField='timestamp', order=-1) {
        console.log("findAssetsBySize>");
        if(empty(fromSize))
            fromSize = 0;
        if(empty(toSize))
            toSize = Number.MAX_SAFE_INTEGER;
        let queryBody = {
            size : { $gte: fromSize, $lte: toSize }
        };
        return this.findAssets(queryBody, page, pageSize, sortField, order);
    }
    /**
     * search assets within timestamp span.
     * @param {Date} fromTime=Date('1970-01-01T00:00:00Z') - min timestamp.
     * @param {Date} toTime=Date.now() - max timestamp.
     * @param {number} page=1 - page number.
     * @param {number} pageSize=10 - the number of documents in each page.
     * @param {string} sortField='timestamp' - field used to sort documents.
     * @param {number} order=[-1,1] - sort order.
     * @returns {Object} documents with pagination
     */
    findAssetsByTimestamp(fromTime=new Date('1970-01-01T00:00:00Z'), toTime=Date.now(), page, pageSize, sortField, order) {
        if(empty(fromTime))
            fromTime = new Date('1970-01-01T00:00:00Z');
        if(empty(toTime))
            toTime = Date.now();
        let queryBody = {
            timestamp : { $gte: fromTime, $lte: toTime }
        };
        return this.findAssets(queryBody, page, pageSize, sortField, order);
    }
    /**
     * search assets by account name.
     * @param {string} accountName - aaa chain account name.
     * @param {number} page=1 - page number.
     * @param {number} pageSize=10 - the number of documents in each page.
     * @param {string} sortField='timestamp' - field used to sort documents.
     * @param {number} order=[-1,1] - sort order.
     * @returns {Object} documents with pagination
     */
    findAssetsByAccount(accountName, page, pageSize, sortField, order) {
        let queryBody = {
            stringMatch : {
                account: accountName
            }
        };
        return this.findAssets(queryBody, page, pageSize, sortField, order);
    }
    /**
     * search assets by block number.
     * @param {number} block - block number.
     * @param {number} page=1 - page number.
     * @param {number} pageSize=10 - the number of documents in each page.
     * @param {string} sortField='timestamp' - field used to sort documents.
     * @param {number} order=[-1,1] - sort order.
     * @returns {Object} documents with pagination
     */
    findAssetsByBlock(block, page, pageSize, sortField, order) {
        let queryBody = {
            block : block
        };
        return this.findAssets(queryBody, page, pageSize, sortField, order);
    }
}
/**
 * DataGridWriterClient provides different interfaces
 * to add new asset into data grid network.
 */
class DataGridWriterClient {
    /**
     * creates a instance of DataGridReaderClient.
     * @param {string} host - server ip address.
     * @param {number} port - server port number.
     */
    constructor(host, port) {
        console.log("DataGridWriterClient > constructor");
        /**
         * server ip address.
         * @type {string} host.
         * @public
         */
        this.host = host;
        /**
         * server port number.
         * @type {number} port.
         * @public
         */
        this.port = port;
    }
    /**
     * add file asset.
     * @param {string} filePath - file path.
     * @param {string} account - aaa chain account name.
     * @param {string} metadata - metadata to describe the asset.
     * @param {string} privateKey - account private key.
     * @param {string} ipfsOptions - storage options.
     * @returns {Object} asset hashIds and thx number
     */
    addFileAsset(filePath, account, metadata, privateKey, ipfsOptions) {
        console.log("addFileAsset>");
        const isString = typeof filePath === "string" || typeof account === "string" || typeof metadata === "string" || typeof privateKey === "string" || typeof ipfsOptions === "string";
        if(isString === false) {
            throw new Error("all parameters must be string!");
        }
        console.log("metadata size: ", jsonSize(JSON.parse(metadata)));
        if(jsonSize(JSON.parse(metadata)) > maxSizeOfMetadataByBytes) { // check metadata size exceed max or not
            throw new Error("metadata size exceeded max size!");
        }
        const readFileStream = fs.createReadStream(filePath);
        const formData = {
            account: account,
            metadata: metadata,
            signature: ecc.sign(metadata, privateKey),
            options: ipfsOptions,
            file: readFileStream
        };
        const url = 'http://' + this.host +':' + this.port + '/add/file';
        return new Promise((resolve, reject) => {
            request.post({
                url: url,
                json: true,
                formData: formData
            }, function optionalCallback(err, httpResponse, body) {
                if (err) {
                    reject(err);
                }
                resolve(body)
            });
        });
    }
    /**
     * add data asset.
     * @param {Object} data - asset source content(json).
     * @param {string} account - aaa chain account name.
     * @param {Object} metadata - metadata to describe the asset.
     * @param {string} privateKey - account private key.
     * @param {Object} ipfsOptions - storage options.
     * @returns {Object} asset hashIds and thx number
     */
    addDataAsset(data, account, metadata, privateKey, ipfsOptions) {
        console.log("addFileAsset>");
        const isString = typeof account === "string" || typeof privateKey === "string";
        const isObject = typeof data === "object" || typeof metadata === "object" || typeof ipfsOptions === "object";
        if(isString && isObject === false) {
            throw new Error("wrong parameter type!");
        }
        console.log("metadata size: ", jsonSize(metadata));
        if(jsonSize(metadata) > maxSizeOfMetadataByBytes) { // check metadata size exceed max or not
            throw new Error("metadata size exceeded max size!");
        }
        const bodyParams = {
            "account": account,
            "metadata": metadata,
            "signature": ecc.sign(JSON.stringify(metadata), privateKey),
            "source": data,
            "options": ipfsOptions
        };
        const url = 'http://' + this.host +':' + this.port + '/add/data';
        return new Promise((resolve, reject) => {
            request.post({
                url: url,
                json: true,
                body: bodyParams
            }, function optionalCallback(err, httpResponse, body) {
                if (err) {
                    reject(err);
                }
                resolve(body);
            });
        });
    }
    updateAssetMetadata(sourceHashId, metadata, account, privateKey, ipfsOptions) {
        console.log("updateAssetMetadata>");
        const bodyParams = {
            "account": account,
            "metadata": metadata,
            "signature": ecc.sign(JSON.stringify(metadata), privateKey),
            "options": ipfsOptions
        };
        const url = 'http://' + this.host +':' + this.port + '/update/' + sourceHashId;
        return new Promise((resolve, reject) => {
            request.post({
                url: url,
                json: true,
                body: bodyParams
            }, function optionalCallback(err, httpResponse, body) {
                if (err) {
                    reject(err);
                }
                resolve(body)
            });
        });
    }
}
export { DataGridReaderClient, DataGridWriterClient };