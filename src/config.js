'use strict';

module.exports = {
    asset: {
        source: {
            sizeLimit: {
                max: 100,
                unit: 'M'
            }
        },
        metadata: {
            sizeLimit: {
                max: 8,
                unit: 'KB'
            }
        }
    },
    ipfs: {
        host: '127.0.0.1',
        port: '5001',
        options: {
            protocol: 'http',
            EXPERIMENTAL: {
                pubsub: true
            }
        }
    },
    mongo: {
        host: '127.0.0.1',
        port: '27017',
        dbname: 'ipfs'
    },
    eos: {
        test: {
            host: '193.93.219.219',
            port: '8888',
            chainId: '038f4b0fc8ff18a4f0842a8f0564611f6e96e8535901dd45e43ac8691a1c4dca',
            keyProvider: ['5K1ZE5seSS3PdxWHjXsRreqtTBEjMBPrL4WTke6Fe7jwf17rHKK'] // existing account (active) private key that has ram cpu and bandwidth already purchased
        },
        main: {
            host: '193.93.219.219',
            port: '8888',
            chainId: '038f4b0fc8ff18a4f0842a8f0564611f6e96e8535901dd45e43ac8691a1c4dca',
            keyProvider: ['5K1ZE5seSS3PdxWHjXsRreqtTBEjMBPrL4WTke6Fe7jwf17rHKK'] // existing account (active) private key that has ram cpu and bandwidth already purchased
        }
    },
    MetadataProducer: {
        privateKey: '5K1ZE5seSS3PdxWHjXsRreqtTBEjMBPrL4WTke6Fe7jwf17rHKK'
    }
};