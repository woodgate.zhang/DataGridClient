'use strict';

import fs from 'fs';
import path from 'path';
import chai from 'chai';
import { DataGridReaderClient, DataGridWriterClient } from '../client';


const expect = chai.expect;
const assert = chai.assert;
const should = chai.should();

const eosPrivateKey = '5K1ZE5seSS3PdxWHjXsRreqtTBEjMBPrL4WTke6Fe7jwf17rHKK';

describe('.client.reader (findAssets)', function () {
    this.timeout(120 * 1000);
    const client = new DataGridReaderClient('127.0.0.1', 3003);
    before((done) => {
        done();
    });
    after((done) => {
        done();
    });
    it('reader.findAssetsBySize.gte', function (done) {
        client.findAssetsBySize(70).then(ret => {
            console.log("server response: ", JSON.stringify(ret));
            ret.should.have.property('docs');
            ret.should.have.property('total');
            ret.should.have.property('page');
            ret.total.should.not.equal(0);
            ret.page.should.equal(1);
            done();
        }).catch(error => {
            done(error);
        });
    });
    it('reader.findAssetsBySize.lte', function (done) {
        client.findAssetsBySize(0, 20).then(ret => {
            console.log("server response: ", JSON.stringify(ret));
            ret.should.have.property('docs');
            ret.should.have.property('total');
            ret.should.have.property('page');
            ret.total.should.equal(0);
            ret.page.should.equal(1);
            done();
        }).catch(error => {
            done(error);
        });
    });
    it('reader.findAssetsBySize.between', function (done) {
        client.findAssetsBySize(50, 70).then(ret => {
            console.log("server response: ", JSON.stringify(ret));
            ret.should.have.property('docs');
            ret.should.have.property('total');
            ret.should.have.property('page');
            ret.total.should.not.equal(0);
            ret.page.should.equal(1);
            done();
        }).catch(error => {
            done(error);
        });
    });
    it('reader.findAssetsByTimestamp.gte', function (done) {
        client.findAssetsByTimestamp(new Date('2018-09-11')).then(ret => {
            console.log("server response: ", JSON.stringify(ret));
            ret.should.have.property('docs');
            ret.should.have.property('total');
            ret.should.have.property('page');
            ret.total.should.not.equal(0);
            ret.page.should.equal(1);
            done();
        }).catch(error => {
            done(error);
        });
    });
    it('reader.findAssetsByTimestamp.lte', function (done) {
        client.findAssetsByTimestamp(null, new Date('2018-09-12')).then(ret => {
            console.log("server response: ", JSON.stringify(ret));
            ret.should.have.property('docs');
            ret.should.have.property('total');
            ret.should.have.property('page');
            ret.total.should.not.equal(0);
            ret.page.should.equal(1);
            done();
        }).catch(error => {
            done(error);
        });
    });
    it('reader.findAssetsByTimestamp.between', function (done) {
        client.findAssetsByTimestamp(new Date('2018-08-12'), new Date('2018-08-13')).then(ret => {
            console.log("server response: ", JSON.stringify(ret));
            ret.should.have.property('docs');
            ret.should.have.property('total');
            ret.should.have.property('page');
            ret.total.should.equal(0);
            ret.page.should.equal(1);
            done();
        }).catch(error => {
            done(error);
        });
    });
    it('reader.findAssetsByAccount', function (done) {
        client.findAssetsByAccount('ming').then(ret => {
            console.log("server response: ", JSON.stringify(ret));
            ret.should.have.property('docs');
            ret.should.have.property('total');
            ret.should.have.property('page');
            ret.total.should.not.equal(0);
            ret.page.should.equal(1);
            done();
        }).catch(error => {
            done(error);
        });
    });
    it('reader.findAssetsByBlock', function (done) {
        client.findAssetsByBlock(13984385).then(ret => {
            console.log("server response: ", JSON.stringify(ret));
            ret.should.have.property('docs');
            ret.should.have.property('total');
            ret.should.have.property('page');
            ret.total.should.not.equal(0);
            ret.page.should.equal(1);
            done();
        }).catch(error => {
            done(error);
        });
    });
});

describe('.client.writer (addFileAsset)', function () {
    this.timeout(120 * 1000);
    const client = new DataGridWriterClient('127.0.0.1', 3002);
    before((done) => {
        done();
    });
    after((done) => {
        done();
    });
    it('writer.addFileAsset', function (done) {
        fs.writeFileSync(path.resolve(__dirname, '../../resource/test-file.txt'), new Date() + Math.random().toString(36).substring(8));
        const filePath = path.resolve(__dirname, '../../resource/test-file.txt');
        const metadata = {"name": "test-file", "desc":"spaceX third launch", "location": "Beijing"};
        const ipfsOptions = {"onlyHash": false};
        console.log(filePath);
        client.addFileAsset(
            filePath, 'mingqi',
            JSON.stringify(metadata),
            eosPrivateKey,
            JSON.stringify(ipfsOptions)
        ).then(ret => {
            console.log("server response: ", JSON.stringify(ret));
            ret.should.have.property('tx');
            ret.should.have.property('source');
            ret.should.have.property('metadata');
            done();
        }).catch(error => {
            done(error);
        });
    });
    it('writer.addFileAsset: metadata exceeds maxSize', function (done) {
        fs.writeFileSync(path.resolve(__dirname, '../../resource/test-file.txt'), new Date() + Math.random().toString(36).substring(8));
        const filePath = path.resolve(__dirname, '../../resource/test-file.txt');
        const bufferedMetadata = fs.readFileSync(path.resolve(__dirname, '../../resource/json_metadata.txt'));
        const metadata = JSON.parse(bufferedMetadata.toString());
        const ipfsOptions = {"onlyHash": false};
        console.log(filePath);
        try {
            client.addFileAsset(
                filePath,
                'mingqi',
                JSON.stringify(metadata),
                eosPrivateKey,
                JSON.stringify(ipfsOptions)
            )
        } catch(error) {
            console.error("Error: ", error.message);
            expect(error.message).to.equal('metadata size exceeded max size!');
            done();
        }
    });
    it('writer.addDataAsset', function (done) {
        const data = {"name":"wangpengpeng","gender":"male","age":28,'random':Date.now() + Math.random().toString(36).substring(7)};
        const metadata = {"name": "wangpengpeng", "desc":"spaceX test", "location": "Beijing"};
        const ipfsOptions = {"onlyHash": false};
        client.addDataAsset(
            JSON.stringify(data),
            'mingqi',
            metadata,
            eosPrivateKey,
            ipfsOptions
        ).then(ret => {
            console.log("server response: ", JSON.stringify(ret));
            ret.should.have.property('tx');
            ret.should.have.property('source');
            ret.should.have.property('metadata');
            done();
        }).catch(error => {
            done(error);
        });
    });
    it('writer.addDataAsset: metadata exceeds maxSize', function (done) {
        const data = {"name":"wangpengpeng","gender":"male","age":28,'random':Date.now() + Math.random().toString(36).substring(7)};
        const bufferedMetadata = fs.readFileSync(path.resolve(__dirname, '../../resource/json_metadata.txt'));
        const metadata = JSON.parse(bufferedMetadata.toString());
        const ipfsOptions = {"onlyHash": false};
        try {
            client.addDataAsset(
                JSON.stringify(data),
                'mingqi',
                metadata,
                eosPrivateKey,
                ipfsOptions
            ).catch(error => {
                done(error);
            });
        } catch(error) {
            console.error("Error: ", error.message);
            expect(error.message).to.equal('metadata size exceeded max size!');
            done();
        }
    });
    it('writer.updateAssetMetadata', function (done) {
        const readerClient = new DataGridReaderClient('127.0.0.1', 3003);
        readerClient.findAssetsByTimestamp(new Date('2018-08-12')).then(ret => {
            console.log("server response: ", JSON.stringify(ret));
            if(ret.docs.length > 0) {
                const lastTHXId = ret.docs[0].thxId;
                const metadata = {"name": "test-update", "desc":"test metadata updating", "location": "Beijing"};
                const ipfsOptions = {"onlyHash": false};
                client.updateAssetMetadata(
                    ret.docs[0].hashId,
                    metadata,
                    'mingqi',
                    eosPrivateKey,
                    ipfsOptions
                ).then(ret => {
                    console.log("server response: ", JSON.stringify(ret));
                    ret.should.have.property('tx');
                    ret.should.have.property('source');
                    ret.should.have.property('metadata');
                    ret.tx.should.not.equal(lastTHXId);
                    done();
                }).catch(error => {
                    done(error);
                });
            } else {
                console.error("no asset detected!");
                done();
            }
        }).catch(error => {
            done(error);
        });
    });
});