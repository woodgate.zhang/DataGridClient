const grunt = require('grunt');
require("load-grunt-tasks")(grunt); // npm install --save-dev load-grunt-tasks

module.exports = function(grunt) {
    grunt.initConfig({
        esdoc : {
            dist : {
                options: {
                    source: './src',
                    destination: './doc',
                    test: {
                        "type": "mocha",
                        "source": "./src/test"
                    }
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-esdoc');
    grunt.registerTask('default', ['esdoc']);
};